import { MpxClientPage } from './app.po';

describe('mpx-client App', () => {
  let page: MpxClientPage;

  beforeEach(() => {
    page = new MpxClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
