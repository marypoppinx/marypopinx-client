import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MaterialModule} from '@angular/material';

describe('AppComponent', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule.withRoutes([]),
      MaterialModule
    ],
    declarations: [
      AppComponent
    ],
    }).compileComponents()
  ));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render toolbar with 2 links', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const buttons = compiled.querySelectorAll('md-toolbar nav button');
    expect(buttons.length).toEqual(2);
    expect(buttons[0].attributes.routerLink.value).toEqual('/home');
    expect(buttons[1].attributes.routerLink.value).toEqual('/login');
  }));
});
