import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpModule, RequestOptions, XHRBackend, Http} from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { ResourceModule } from 'ngx-resource';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MdInputModule, MdButtonModule, MdToolbarModule, MdIconModule, MdIconRegistry, MdCardModule,
  MdProgressSpinnerModule, MdChipsModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { MpxHomeComponent } from '../mpx-home/mpx-home.component';
import { MpxLoginComponent } from '../mpx-login/mpx-login.component';
import {SubtitleParserService} from '../../services/subtitle-parser/subtitle-parser.service';
import {SafePipe} from '../../pipes/safe/safe.pipe';
import {MpxLoginSuccessComponent} from '../mpx-login-success/mpx-login-success.component';
import {LocalStorageModule, LocalStorageService} from 'angular-2-local-storage';
import {HttpFactory} from '../../services/mpx-http/mpx-http.factory';

const appRoutes: Routes = [
  { path: 'home', component: MpxHomeComponent },
  { path: 'login', component: MpxLoginComponent },
  { path: 'login-success', component: MpxLoginSuccessComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MpxHomeComponent,
    MpxLoginComponent,
    MpxLoginSuccessComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    ResourceModule.forRoot(),
    BrowserAnimationsModule,
    FlexLayoutModule,
    MdInputModule,
    MdButtonModule,
    MdToolbarModule,
    MdIconModule,
    MdCardModule,
    MdProgressSpinnerModule,
    MdChipsModule,
    LocalStorageModule.withConfig({
      prefix: 'mpx',
      storageType: 'localStorage'
    })
  ],
  providers: [
    MdIconRegistry,
    SubtitleParserService,
    {
      provide: Http,
      useFactory: HttpFactory,
      deps: [XHRBackend, RequestOptions, LocalStorageService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
