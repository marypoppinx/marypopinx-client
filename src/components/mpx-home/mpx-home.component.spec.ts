import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpxHomeComponent } from './mpx-home.component';
import {MaterialModule} from '@angular/material';
import {SafePipe} from '../../pipes/safe/safe.pipe';
import {FormsModule} from '@angular/forms';
import {MpxVideoResource} from '../../services/mpx-video-resource/mpx-video-resource';
import {SubtitleParserService} from '../../services/subtitle-parser/subtitle-parser.service';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('MpxHomeComponent', () => {
  let component: MpxHomeComponent;
  let fixture: ComponentFixture<MpxHomeComponent>;

  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      MaterialModule,
      FormsModule,
      NoopAnimationsModule
    ],
    declarations: [
      MpxHomeComponent,
      SafePipe
    ],
    providers: [{
      provide: MpxVideoResource,
      useValue: {
      }
    }, {
      provide: SubtitleParserService,
      useValue: {
        parse: () => []
      }
    }]
  }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpxHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
