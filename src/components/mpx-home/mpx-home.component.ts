import { Component, OnInit } from '@angular/core';
import {MpxVideoResource} from '../../services/mpx-video-resource/mpx-video-resource';
import {MpxVideo} from '../../domain/mpx-video';
import {SubtitleParserService} from '../../services/subtitle-parser/subtitle-parser.service';
import {map} from 'lodash';

@Component({
  selector: 'app-mpx-home',
  templateUrl: './mpx-home.component.html',
  styleUrls: ['./mpx-home.component.scss']
})
export class MpxHomeComponent implements OnInit {

  public searchQuery: string;
  public searchInProgress: boolean = false;
  public videos: Array<{item: MpxVideo, entries: any[]}>;

  constructor(
    private videoResource: MpxVideoResource,
    private subtitleParser: SubtitleParserService
  ) { }

  ngOnInit() {
    this.searchQuery = '';
  }

  search(searchQuery: string): void {
    this.searchInProgress = true;
    this.videoResource.query({words: searchQuery}).$observable.subscribe(result => {
      this.searchInProgress = false;
      this.videos = map(result, item => {
        return {
          item,
          entries: this.subtitleParser.parse(item.subtitles, searchQuery.split(/\s+/))
        }
      });
      console.log(this.videos);
    })
  }

  toDate(seconds: number): Date {
    const date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(seconds);
    date.setMilliseconds(0);
    return date;
  }
}
