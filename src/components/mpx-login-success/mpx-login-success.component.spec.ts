import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpxLoginSuccessComponent } from './mpx-login-success.component';

describe('MpxLoginSuccessComponent', () => {
  let component: MpxLoginSuccessComponent;
  let fixture: ComponentFixture<MpxLoginSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpxLoginSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpxLoginSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
