import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-mpx-login',
  templateUrl: './mpx-login-success.component.html',
  styleUrls: ['./mpx-login-success.component.scss']
})
export class MpxLoginSuccessComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private localStorageService: LocalStorageService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      const accessToken = params['access_token'];
      console.log(accessToken);
      this.localStorageService.add('accessToken', accessToken);
      return this.router.navigate(['/home']);
    });
  }

}
