import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpxLoginComponent } from './mpx-login.component';
import {MdIconModule} from '@angular/material';
import {HttpModule} from '@angular/http';

describe('MpxLoginComponent', () => {
  let component: MpxLoginComponent;
  let fixture: ComponentFixture<MpxLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MdIconModule, HttpModule],
      declarations: [ MpxLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpxLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
