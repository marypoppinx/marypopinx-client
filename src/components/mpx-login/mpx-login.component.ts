import { Component, OnInit } from '@angular/core';
import {MdIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-mpx-login',
  templateUrl: './mpx-login.component.html',
  styleUrls: ['./mpx-login.component.scss']
})
export class MpxLoginComponent implements OnInit {

  constructor(private iconRegistry: MdIconRegistry, private sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('facebook-box', sanitizer.bypassSecurityTrustResourceUrl('assets/images/facebook-box.svg'));
  }

  ngOnInit() {
  }

}
