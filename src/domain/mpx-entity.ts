import {autoserialize} from "cerialize";

export class MpxEntity {
  @autoserialize id: string;
  @autoserialize createdAt: number;
  @autoserialize updatedAt: number;
}
