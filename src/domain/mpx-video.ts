import {MpxEntity} from './mpx-entity';
import {inheritSerialization, autoserialize, autoserializeAs} from 'cerialize';

class Thumbnail {
  @autoserialize height: number;
  @autoserialize width: number;
  @autoserialize url: string;
}
class Thumbnails {
  @autoserializeAs(Thumbnail) default: Thumbnail;
  @autoserializeAs(Thumbnail) high: Thumbnail;
  @autoserializeAs(Thumbnail) maxres: Thumbnail;
  @autoserializeAs(Thumbnail) medium: Thumbnail;
  @autoserializeAs(Thumbnail) standard: Thumbnail;
}

@inheritSerialization(MpxEntity)
export class MpxVideo extends MpxEntity {
  @autoserialize type: string;
  @autoserialize score: number;
  @autoserialize originalUrl: string;
  @autoserialize title: string;
  @autoserialize description: string;
  @autoserialize subtitles: string;
  @autoserializeAs(Thumbnails) thumbnails: Thumbnails;
}
