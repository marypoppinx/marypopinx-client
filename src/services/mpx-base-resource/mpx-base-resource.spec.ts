import { TestBed, inject } from '@angular/core/testing';

import {IQueryInput, MpxBaseResource} from './mpx-base-resource';
import {MpxEntity} from '../../domain/mpx-entity';
import {HttpModule} from '@angular/http';

describe('MpxBaseResource', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [MpxBaseResource]
    });
  });

  it('should be created', inject([MpxBaseResource], (service: MpxBaseResource<IQueryInput, MpxEntity>) => {
    expect(service).toBeTruthy();
  }));
});
