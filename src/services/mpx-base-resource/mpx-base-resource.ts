import {Resource, ResourceAction, ResourceMethod} from "ngx-resource";
import {RequestMethod} from "@angular/http";
import {MpxEntity} from '../../domain/mpx-entity';

export interface IQueryInput {
  where?: Object,
  limit?: number,
  skip?: number,
  sort?: string,
  select?: string,
  omit?: string
}

export class MpxBaseResource<TQueryInput, TEntity extends MpxEntity> extends Resource {

  @ResourceAction({
    method: RequestMethod.Get,
    isArray: true
  })
  query: ResourceMethod<TQueryInput, TEntity[]>;

  @ResourceAction({
    method: RequestMethod.Post
  })
  create: ResourceMethod<TEntity, TEntity>;

  @ResourceAction({
    method: RequestMethod.Patch,
    path: '/{!id}'
  })
  update: ResourceMethod<TEntity, TEntity>;

  @ResourceAction({
    method: RequestMethod.Delete,
    path: '/{!id}'
  })
  remove: ResourceMethod<TEntity, TEntity>;

  save(entity: TEntity) {
    if (entity.id) {
      return this.update(entity);
    } else {
      return this.create(entity);
    }
  }
}
