import { XHRBackend, RequestOptions } from '@angular/http';

import { MpxHttp } from './mpx-http';
import {LocalStorageService} from 'angular-2-local-storage';

export function HttpFactory(backend: XHRBackend, options: RequestOptions, localStorageService: LocalStorageService) {
  return new MpxHttp(backend, options, localStorageService);
}
