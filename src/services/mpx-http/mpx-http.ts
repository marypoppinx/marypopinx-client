import {Injectable} from '@angular/core';
import {Http, RequestOptionsArgs, Request, Response, ConnectionBackend, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {LocalStorageService} from 'angular-2-local-storage';

@Injectable()
export class MpxHttp extends Http {

  constructor(
    private backend: ConnectionBackend,
    private defaultOptions: RequestOptions,
    private localStorageService: LocalStorageService
  ) {
    super(backend, defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    const accessToken = this.localStorageService.get('accessToken');
    if (accessToken) {
      if (url instanceof Request) {
        url.headers.append('Authorization', `Bearer ${accessToken}`);
      } else {
        options = options || {headers: new Headers()};
        options.headers.append('Authorization', `Bearer ${accessToken}`);
      }
    }
    return super.request(url, options);
  }
}
