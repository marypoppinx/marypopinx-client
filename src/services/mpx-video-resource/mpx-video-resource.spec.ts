import { TestBed, inject } from '@angular/core/testing';

import { MpxVideoResource } from './mpx-video-resource';
import {HttpModule} from '@angular/http';

describe('MpxVideoResource', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [MpxVideoResource]
    });
  });

  it('should be created', inject([MpxVideoResource], (service: MpxVideoResource) => {
    expect(service).toBeTruthy();
  }));
});
