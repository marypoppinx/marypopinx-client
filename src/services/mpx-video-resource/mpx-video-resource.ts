import { Injectable } from '@angular/core';
import {IQueryInput, MpxBaseResource} from '../mpx-base-resource/mpx-base-resource';
import {MpxVideo} from '../../domain/mpx-video';
import {ResourceParams} from 'ngx-resource';
import {Deserialize} from 'cerialize';

interface IVideoQueryInput extends IQueryInput {
  words: string;
}

@Injectable()
@ResourceParams({
  url: 'http://localhost:1337/api/videos'
})
export class MpxVideoResource extends MpxBaseResource<IVideoQueryInput, MpxVideo> {
  initResultObject() {
    return new MpxVideo();
  }

  map(item: any) {
    return Deserialize(item, MpxVideo);
  }
}
