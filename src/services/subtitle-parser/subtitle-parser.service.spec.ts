import { TestBed, inject } from '@angular/core/testing';

import { SubtitleParserService } from './subtitle-parser.service';

describe('SubtitleParserService', () => {
  beforeAll(() => {
    this.subtitles = [
      'WEBVTT',
      'Kind: captions',
      'Langage:  en',
      '',
      '00:00:03.950 --> 00:00:04.350 align:start position:0%',
      'somewords',
      '',
      '00:00:04.350 --> 00:00:06.750 align:start position:0%',
      'text text specific',
      'text text abnormal',
      'text text insane',
      '',
      '00:00:06.750 --> 00:00:09.150 align:start position:0%',
      'somewords',
      'text abnormal',
    ].join('\n');
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubtitleParserService]
    });
  });

  it('should be created', inject([SubtitleParserService], (service: SubtitleParserService) => {
    expect(service).toBeTruthy();
  }));

  it('should return []', inject([SubtitleParserService], (service: SubtitleParserService) => {
    expect(service.parse(this.subtitles, ['unknown'])).toEqual([]);
  }));

  it('should return array of words with timestamps []', inject([SubtitleParserService], (service: SubtitleParserService) => {
    expect(service.parse(this.subtitles, ['abnormal', 'insane'])).toEqual([
      {
        word: 'abnormal',
        timestamps: [4, 6]
      },
      {
        word: 'insane',
        timestamps: [4]
      }
    ]);
  }));
});
