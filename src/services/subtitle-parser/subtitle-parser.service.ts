import { Injectable } from '@angular/core';
import {WebVTT} from 'vtt.js';
import * as stemmer from 'stemmer';

@Injectable()
export class SubtitleParserService {

  parse(subtitles, words) {
    const parser = new WebVTT.Parser(window, WebVTT.StringDecoder());
    const stemsMap = this.prepareStemsMap(words);
    const wordsMap = {};

    parser.oncue = this.parseCue.bind(this, wordsMap, stemsMap);
    parser.onparsingerror = function(e) {
      console.log(e);
    };

    parser.parse(subtitles);
    parser.flush();

    return this.transformWordsMapToArray(wordsMap);
    // return map(wordsMap, (word, key, wordsMap) => {
    //   return {
    //     word,
    //     timestamps: wordsMap[key]
    //   }
    // });
  }

  private splitWords(text) {
    return text.replace(/\s+/g, ' ').split(' ');

  }

  private prepareStemsMap(words) {
    return words.reduce((stemsMap, word) => {
      stemsMap[stemmer(word)] = word;

      return stemsMap;
    }, {});
  }

  private parseCue(wordsMap, stemsMap, {text, startTime}) {
    this.splitWords(text).map((word) => {
      const searchedWord = stemsMap[stemmer(word)];

      if (searchedWord) {
        if (!wordsMap[searchedWord]) {
          wordsMap[searchedWord] = [];
        }

        wordsMap[searchedWord].push(Math.floor(startTime));
      }
    });
  }

  private transformWordsMapToArray(map) {
    const array = [];

    for (let word in map) {
      array.push({word, timestamps: map[word]});
    }

    return array;
  }
}
